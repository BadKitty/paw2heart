<?php

/**
 * The plugin bootstrap file
 *
 * @link              http://mykatieblue.com
 * @since             1.0.0
 * @package           paw2heart
 *
 * @wordpress-plugin
 * Plugin Name:       Paw2Heart
 * Plugin URI:        http://mykatieblue.com/paw2heart/
 * Description:       This plugin handles the Paw2Heart feature of mykatieblue.com.
 * Version:           1.0.0
 * Author:            Kelly Carter
 * Author URI:        http://rainworkswebdevelopment.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       paw2heart
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-paw2heart-activator.php
 */
function activate_paw2heart() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-paw2heart-activator.php';
	Paw2Heart_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-paw2heart-deactivator.php
 */
function deactivate_paw2heart() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-paw2heart-deactivator.php';
	Paw2Heart_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_paw2heart' );
register_deactivation_hook( __FILE__, 'deactivate_paw2heart' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-paw2heart.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_paw2heart() {

	$plugin = new Paw2Heart();
	$plugin->run();

}
run_paw2heart();
