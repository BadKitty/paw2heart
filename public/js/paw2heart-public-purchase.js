(function( $ ) {
	'use strict';

	$(function () {
		$("form#paw2heart-purchase input[name=preview]").click(function() {
			$("form#paw2heart-purchase input[name=action]").val("preview");
			$("form#paw2heart-purchase").submit();
		});
	});

})( jQuery );
