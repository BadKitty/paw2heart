<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://mykatieblue.com
 * @since      1.0.0
 *
 * @package    Paw2Heart
 * @subpackage Paw2Heart/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Paw2Heart
 * @subpackage Paw2Heart/public
 * @author     Kelly Carter <kelly@rainworksweb.com>
 */
class Paw2Heart_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Paw2Heart_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Paw2Heart_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/paw2heart-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Paw2Heart_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Paw2Heart_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/paw2heart-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Run the shortcode to display the Paw2Heart cards.
	 *
	 * @since    1.0.0
	 */
	public function do_listing_shortcode() {
		return $this->show_partial('list');
	}

	/**
	 * Run the shortcode to display the purchase form.
	 *
	 * @since    1.0.0
	 */
	public function do_purchase_shortcode() {
		$purchase = new Paw2Heart_Purchase();
		$plugin_card_post_type = new Paw2Heart_Card_Post_Type($_POST);
		$clean_data = $plugin_card_post_type->card_data;
		
		if ($_POST && !wp_verify_nonce($_POST['_wpnonce'], 'paw2heart-purchase-options')) {
			return $this->show_partial('purchase', $clean_data);
		}
		
		if ($_POST && $_POST['action'] == 'preview' && $_POST['photo']) {
			$image = $purchase->process_photo($_POST['photo']);
			
		}
		
		$clean_data['card_post_id'] = $plugin_card_post_type->create();
		
		return $this->show_partial('purchase', $clean_data);
	}

	/**
	 * Display a partial. The 'list' partial is selected by default.
	 *
	 * @since    1.0.0
	 */
	private function show_partial($template = 'list', $args = null) {
		//ob_start();
		//echo '<pre>'.print_r($args, true) . '</pre>';
		//extract($args, EXTR_SKIP);
	
		include plugin_dir_path( dirname( __FILE__ ) ) . "/public/partials/paw2heart-public-display-$template.php";
		
		//ob_end_clean();
		//return $content;
	}

}
