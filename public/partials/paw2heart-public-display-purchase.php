<?php

/**
 * Provide a public-facing view for the plugin's purchase page
 *
 * This file is used to markup the public-facing aspects of the plugin's purchase functionality.
 *
 * @link       http://mykatieblue.com
 * @since      1.0.0
 *
 * @package    Paw2Heart
 * @subpackage Paw2Heart/public/partials
 */
?>

<pre><?php print_r($args); ?></pre>
<?php if (!empty($args['card_post_id']) && $args['card_post_id'] > 0) { ?>
<div id="paw2heart-success">
	<p>Success! Thank you for your purchase. You may view your new Paw2Heart card <a href="<?php get_permalink($args['card_post_id']) ?>">here</a>.</p>
</div>
<?php } else if (!empty($args['_wpnonce'])) { ?>
<div id="paw2heart-error">
	<p>There is an error in your input.</p>
</div>
<?php } ?>

<form method="post" id="paw2heart-purchase">
	<?php wp_nonce_field( 'paw2heart-purchase-options' ); ?>
	<input type="hidden" name="action" value="purchase" />
	<label>
		Your First Name:
		<input type="text" name="owner_first_name" value="<?php echo $args['owner_first_name'] ?>" required />
	</label>
	<label>
		Your Pet's Name:
		<input type="text" name="pet_name" value="<?php echo $args['pet_name'] ?>" required />
	</label>
	<label>
		Your Pet's Date of Death:
		<input type="text" name="pet_date" value="<?php echo $args['pet_date'] ?>" required />
	</label>
	<label>
		Your Pet's Breed:
		<select name="pet_breed" required>
			<option></option>
			<?php Paw2Heart_Purchase::get_pet_options_html('_dog_breeds', $args['pet_breed']); ?>
		</select>
	</label>
	<label>
		Photo of Your Pet:
		<input type="file" name="photo" required />
		<input type="button" name="preview" value="Preview" />
	</label>
	<label>
		Your Choice of Frame:
		<select name="frame_id" required>
			<option></option>
			<?php Paw2Heart_Purchase::get_pet_options_html('_frames', $args['frame_id']); ?>
		</select>
	</label>
	<div class="buttons">
		<input type="submit" value="Continue to PayPal" />
	</div>
</form>