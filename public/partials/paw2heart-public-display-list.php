<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://mykatieblue.com
 * @since      1.0.0
 *
 * @package    Paw2Heart
 * @subpackage Paw2Heart/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->