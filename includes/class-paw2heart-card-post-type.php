<?php
//See: https://wordpress.org/support/topic/fatal-error-call-to-undefined-function-wp_verify_nonce
require_once(ABSPATH .'wp-includes/pluggable.php');
session_start();

/**
 * Paw2Heart Card Post Type and related functionality
 * @author Kelly Carter <kelly@rainworksweb.com>
 * @link http://mykatieblue.com
 */
class Paw2Heart_Card_Post_Type {

    /**
     * The name of the post type.
     * @var string
     */
    public $post_type_name = "card";
	
	public $card_data = array(
		"owner_first_name" => '',
		"pet_name" => '',
		"pet_date" => '',
		"pet_breed" => '',
		"phrase" => '',
		"back_text" => '',
		"photo_uri" => '',
		"card_image_uri" => '',
		"frame_id" => '',
		"purchase_amount" => 0,
		"transaction_id" => ''
	);

    /**
     * Sets default values, registers the passed post type, and
     * listens for when the post is saved.
     *
     * @param string $name The name of the desired post type.
     * @param array @post_type_args Override the options.
     */
    function __construct($post_data = null) {
		if ($post_data) {
			$this->card_data = array_merge($this->card_data, $this->sanitize_data($post_data));
		}
		
        $this->save_post();
    }
	
	function create() {
		if(!$this->data_is_valid()) {
			return 0;
		}
		
		$new_post_id = wp_insert_post(array(
			"post_title" => $this->card_data["pet_name"],
			"post_author" => 1,
			"post_content" => $this->card_data["back_text"],
			"post_status" => "publish",
			"post_type" => "card",
			"meta_input" => array(
				"paw2heart_pet_date" => $this->card_data["pet_date"],
				"paw2heart_pet_breed" => $this->card_data["pet_breed"],
				"paw2heart_phrase" => $this->card_data["pet_phrase"],
				"paw2heart_frame_id" => $this->card_data["frame_id"],
				"paw2heart_purchase_amount" => $this->card_data["purchase_amount"],
				"paw2heart_transaction_id" => $this->card_data["transaction_id"]
			)
		));
		echo $new_post_id;
		
		return $new_post_id;
	}

    /**
     * Helper method, that attaches a passed function to the 'admin_init' WP action
     * @param function $cb Passed callback function.
     */
    function admin_init($cb) {
        add_action("admin_init", $cb);
    }

    /**
     * Registers a new post type in the WP db.
     */
    function register_post_type() {
        $n = ucwords($this->post_type_name);

        $args = array(
            "labels" => array(
				"name" => __("Paw2Heart Cards"),
				"singular_name" => __('Paw2Heart Card'),
				"add_new_item" => __("Add New Card"),
				"edit_item" => __("Edit Card"),
				"new_item" => __("New Card"),
				"search_items" => __("Search Cards"),
				"not_found" => __("No cards found"),
				"not_found_in_trash" => __("No cards found in Trash"),
				"all_items" => __("All Cards"),
				"archives" => __("Card Archives"),
				"insert_into_item" => __("Insert into card"),
				"uploaded_to_this_item" => __("Uploaded to this card"),
				"featured_image" => __("Card image"),
				"set_featured_image" => __("Set card image"),
				"remove_featured_image" => __("Remove card image"),
				"use_featured_image" => __("Use as card image")
			),
            "public" => true,
            "publicly_queryable" => true,
            "query_var" => true,
            "menu_icon" => "dashicons-images-alt",
            "hierarchical" => false,
            "menu_position" => 25,
            "supports" => array("title", "editor", "thumbnail", "custom-fields"),
            "has_archive" => true
        );

        register_post_type($this->post_type_name, $args);
    }

    /**
     * Creates a new custom meta box in the New 'post_type' page.
     *
     * @param string $title
     * @param array $form_fields Associated array that contains the label of the input, and the desired input type. 'Title' => 'text'
     */
    function add_meta_box($title, $form_fields = array()) {
        $post_type_name = $this->post_type_name;

        // end update_edit_form
        add_action('post_edit_form_tag', function() {
			echo ' enctype="multipart/form-data"';
		});

        // At WordPress' admin_init action, add any applicable metaboxes.
        $this->admin_init(function() use($title, $form_fields, $post_type_name) {
			add_meta_box(
				strtolower(str_replace(' ', '_', $title)), // id
				$title, // title
				function($post, $data)
				{ // function that displays the form fields
					global $post;

					wp_nonce_field(plugin_basename(__FILE__), 'card_nonce');

					// List of all the specified form fields
					$inputs = $data['args'][0];

					// Get the saved field values
					$meta = get_post_custom($post->ID);

					// For each form field specified, we need to create the necessary markup
					// $name = Label, $type = the type of input to create
					foreach ($inputs as $name => $type) {
						#'Happiness Info' in 'Snippet Info' box becomes
						# snippet_info_happiness_level
						$id_name = $data['id'] . '_' . strtolower(str_replace(' ', '_', $name));

						if (is_array($inputs[$name])) {
							// then it must be a select or file upload
							// $inputs[$name][0] = type of input

							if (strtolower($inputs[$name][0]) === 'select') {
								// filter through them, and create options
								$select = "<select name='$id_name' class='widefat'>";
								foreach ($inputs[$name][1] as $option) {
									// if what's stored in the db is equal to the
									// current value in the foreach, that should
									// be the selected one

									if (isset($meta[$id_name]) && $meta[$id_name][0] == $option) {
										$set_selected = "selected='selected'";
									} else $set_selected = '';

									$select .= "<option value='$option' $set_selected> $option </option>";
								}
								$select .= "</select>";
								array_push($_SESSION['taxonomy_data'], $id_name);
							}
						}

						// Attempt to set the value of the input, based on what's saved in the db.
						$value = isset($meta[$id_name][0]) ? $meta[$id_name][0] : '';

						$checked = ($type == 'checkbox' && !empty($value) ? 'checked' : '');

						// Sorta sloppy. I need a way to access all these form fields later on.
						// I had trouble finding an easy way to pass these values around, so I'm
						// storing it in a session. Fix eventually.
						array_push($_SESSION['taxonomy_data'], $id_name);

						// TODO - Add the other input types.
						$lookup = array(
							"text" => "<input type='text' name='$id_name' value='$value' class='widefat' />",
							"textarea" => "<textarea name='$id_name' class='widefat' rows='10'>$value</textarea>",
							"checkbox" => "<input type='checkbox' name='$id_name' value='$name' $checked />",
							"select" => isset($select) ? $select : '',
							"file" => "<input type='file' name='$id_name' id='$id_name' />"
						);
						?>

						<p>
							<label><?php echo ucwords($name) . ':'; ?></label>
							<?php echo $lookup[is_array($type) ? $type[0] : $type]; ?>
						</p>

						<p>

							<?php
								// If a file was uploaded, display it below the input.
								$file = get_post_meta($post->ID, $id_name, true);
								if ( $type === 'file' ) {
									// display the image
									$file = get_post_meta($post->ID, $id_name, true);

									$file_type = wp_check_filetype($file);
									$image_types = array('jpeg', 'jpg', 'bmp', 'gif', 'png');
									if ( isset($file) ) {
										if ( in_array($file_type['ext'], $image_types) ) {
											echo "<img src='$file' alt='' style='max-width: 400px;' />";
										} else {
											echo "<a href='$file'>$file</a>";
										}
									}
								}
							?>
						</p>

						<?php

					}
				},
				$post_type_name, // associated post type
				'normal', // location/context. normal, side, etc.
				'default', // priority level
				array($form_fields) // optional passed arguments.
			); // end add_meta_box
		});
    }

    /**
     * When a post saved/updated in the database, this methods updates the meta box params in the db as well.
     */
    function save_post() {
		// Only do the following if we physically submit the form,
		// and now when autosave occurs.
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

		global $post;

		if ($_POST && !wp_verify_nonce($_POST['card_nonce'], plugin_basename(__FILE__))) {
			return;
		}

		// Get all the form fields that were saved in the session,
		// and update their values in the db.
		if (isset($_SESSION['taxonomy_data'])) {
			foreach ($_SESSION['taxonomy_data'] as $form_name) {
				if (!empty($_FILES[$form_name]) ) {
					if ( !empty($_FILES[$form_name]['tmp_name']) ) {
						$upload = wp_upload_bits($_FILES[$form_name]['name'], null, file_get_contents($_FILES[$form_name]['tmp_name']));

						if (isset($upload['error']) && $upload['error'] != 0) {
							wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
						} else {
							update_post_meta($post->ID, $form_name, $upload['url']);
						}
					}
			   } else {
					// Make better. Have to do this, because I can't figure
					// out a better way to deal with checkboxes. If deselected,
					// they won't be represented here, but I still need to
					// update the value to false to blank in the table. Hmm...
					if (!isset($_POST[$form_name])) $_POST[$form_name] = '';
					if (isset($post->ID) ) {
						update_post_meta($post->ID, $form_name, $_POST[$form_name]);
					}
				}
			}

			$_SESSION['taxonomy_data'] = array();
		}
    }
	
	function process_photo($file) {
		
	}
	
	private function sanitize_data($post) {
		foreach($post as $data) {
			if (strpos($data, '_wp') == -1) {
				$data = sanitize_text_field($data);
			}
		}
		return $post;
	}
	
	private function data_is_valid() {
		if (empty($this->card_data['owner_first_name']) || empty($this->card_data['pet_name']) || empty($this->card_data['pet_date']) ||
			/*empty($this->card_data['photo_uri']) ||*/ empty($this->card_data['frame_id'])) {
			return false;
		}
		return true;
	}
}