<?php
/**
 * Paw2Heart Purchase handling
 * @author Kelly Carter <kelly@rainworksweb.com>
 * @link http://mykatieblue.com
 */

class Paw2Heart_Purchase {
	
	function __construct() {
		
	}
	
	public function process_photo($file) {
		$image = new Imagick($file);
		$frame = new Imagick(get_home_path() . 'wp_content/uploads/paw2heart/frames/2000x1800rev-FF99CC.png');
		return $file;
	}
	
	public static function get_pet_options_html($option, $selected_value) {
		$values = get_option('paw2heart' . $option);
		foreach($values as $value) {
			$trimmed = trim($value);
			$selected = $trimmed == $selected_value ? ' selected' : '';
			echo "<option$selected>$trimmed</option>";
		}
	}
}