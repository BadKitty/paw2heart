<?php

/**
 * Fired during plugin activation
 *
 * @link       http://mykatieblue.com
 * @since      1.0.0
 *
 * @package    Paw2Heart
 * @subpackage Paw2Heart/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Paw2Heart
 * @subpackage Paw2Heart/includes
 * @author     Kelly Carter <kelly@rainworksweb.com>
 */
class Paw2Heart_Activator {

	/**
	 * Initialize activation.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		add_option( "paw2heart_db_version", "1.0.0" );
		Paw2Heart_Activator::create_tables();
		Paw2Heart_Activator::populate_tables();
		Paw2Heart_Activator::create_directories();
	}

	/**
	 * Create all necessary database tables.
	 *
	 * @since    1.0.0
	 */
	private static function create_tables() {
		global $wpdb;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		
		$charset_collate = $wpdb->get_charset_collate();
		//$opts_table_name = $wpdb->prefix . 'paw2heart_opts';
		$frames_table_name = $wpdb->prefix . 'paw2heart_frames';
		$breeds_table_name = $wpdb->prefix . 'paw2heart_breeds';
		$phrases_table_name = $wpdb->prefix . 'paw2heart_phrases';
		$cards_table_name = $wpdb->prefix . 'paw2heart_cards';
		$media_table_name = $wpdb->prefix . 'posts';
		/*
		$sql = "CREATE TABLE IF NOT EXISTS $opts_table_name (
			option_id mediumint(9) NOT NULL AUTO_INCREMENT,
			option_name varchar(191) NOT NULL,
			option_value longtext NOT NULL,
			PRIMARY KEY id (option_id)
		) ENGINE = MyISAM $charset_collate;";
		dbDelta( $sql );
		*/
		$sql = "CREATE TABLE IF NOT EXISTS $frames_table_name (
			frame_id mediumint(9) NOT NULL AUTO_INCREMENT,
			frame_name varchar(100) NOT NULL,
			frame_premium bit NOT NULL DEFAULT 0,
			media_id bigint(20),
			FOREIGN KEY (media_id) REFERENCES $media_table_name(ID),
			PRIMARY KEY id (frame_id)
		) ENGINE = MyISAM $charset_collate;";
		dbDelta( $sql );
		
		$sql = "CREATE TABLE IF NOT EXISTS $breeds_table_name (
			breed_id mediumint(9) NOT NULL AUTO_INCREMENT,
			breed_name varchar(100) NOT NULL,
			PRIMARY KEY id (breed_id)
		) ENGINE = MyISAM $charset_collate;";
		dbDelta( $sql );
		
		$sql = "CREATE TABLE IF NOT EXISTS $phrases_table_name (
			phrase_id mediumint(9) NOT NULL AUTO_INCREMENT,
			phrase_text varchar(255) NOT NULL,
			PRIMARY KEY id (phrase_id)
		) ENGINE = MyISAM $charset_collate;";
		dbDelta( $sql );
		
		$sql = "CREATE TABLE IF NOT EXISTS $cards_table_name (
			card_id mediumint(9) NOT NULL AUTO_INCREMENT,
			card_image_loc varchar(255) NOT NULL,
			card_purchase_amount double NOT NULL DEFAULT 0,
			card_transaction_id varchar(255),
			card_pet_name varchar(100),
			card_pet_date varchar(100),
			card_pet_owner_name varchar(100),
			card_back varchar(255),
			frame_id mediumint(9) NOT NULL,
			breed_id mediumint(9) NOT NULL,
			phrase_id mediumint(9) NOT NULL,
			created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY id (card_id),
			FOREIGN KEY (frame_id) REFERENCES $frames_table_name(frame_id) ON UPDATE CASCADE ON DELETE RESTRICT,
			FOREIGN KEY (breed_id) REFERENCES $breeds_table_name(breed_id) ON UPDATE CASCADE ON DELETE RESTRICT,
			FOREIGN KEY (phrase_id) REFERENCES $phrases_table_name(phrase_id) ON UPDATE CASCADE ON DELETE RESTRICT
		) ENGINE = MyISAM $charset_collate;";
		dbDelta( $sql );
	}

	/**
	 * Populate tables with necessary data.
	 *
	 * @since    1.0.0
	 */
	private static function populate_tables() {
		global $wpdb;
		/*
		$frames_table_name = $wpdb->prefix . 'paw2heart_frames';
		$breeds_table_name = $wpdb->prefix . 'paw2heart_frames';
		$cards_table_name = $wpdb->prefix . 'paw2heart_frames';

		$wpdb->insert( 
			$frames_table_name, 
			array(
				'frame_name' => _name,
				'frame_premium' => 0,
				'media_id' => 
			)
		);
		*/
	}

	private static function create_directories() {
		$upload_dir = wp_upload_dir();
		$upload_dir = $upload_dir['basedir'];
		
		if (!file_exists($upload_dir . 'paw2heart')) {
			mkdir(get_home_path() . 'wp_content/uploads/paw2heart', 0777, true);
		}
		if (!file_exists($upload_dir . 'paw2heart/frames')) {
			mkdir(get_home_path() . 'wp_content/uploads/paw2heart/frames', 0777, true);
		}
		if (!file_exists($upload_dir . 'paw2heart/cards')) {
			mkdir(get_home_path() . 'wp_content/uploads/paw2heart/cards', 0777, true);
		}
	}
}
