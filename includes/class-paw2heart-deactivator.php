<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://mykatieblue.com
 * @since      1.0.0
 *
 * @package    Paw2Heart
 * @subpackage Paw2Heart/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Paw2Heart
 * @subpackage Paw2Heart/includes
 * @author     Kelly Carter <kelly@rainworksweb.com>
 */
class Paw2Heart_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $wpdb;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		
		echo get_option('paw2heart_remove_tables');
		if (get_option('paw2heart_remove_tables') == 1) {
			/*
			$opts_table_name = $wpdb->prefix . 'paw2heart_opts';
			$frames_table_name = $wpdb->prefix . 'paw2heart_frames';
			//$breeds_table_name = $wpdb->prefix . 'paw2heart_breeds';
			//$phrases_table_name = $wpdb->prefix . 'paw2heart_phrases';
			//$cards_table_name = $wpdb->prefix . 'paw2heart_cards';

			$sql = "DROP TABLE IF TABLE EXISTS $opts_table_name;"
					. "DROP TABLE IF TABLE EXISTS $frames_table_name;";
			dbDelta( $sql );
			*/
		}
	}

}
