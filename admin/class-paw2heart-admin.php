<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://mykatieblue.com
 * @since      1.0.0
 *
 * @package    Paw2Heart
 * @subpackage Paw2Heart/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Paw2Heart
 * @subpackage Paw2Heart/admin
 * @author     Kelly Carter <kelly@rainworksweb.com>
 */
class Paw2Heart_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Paw2Heart_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Paw2Heart_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/paw2heart-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Paw2Heart_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Paw2Heart_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/paw2heart-admin.js', array( 'jquery' ), $this->version, false );

	}
	
	/**
	 * Add an options page under the Settings submenu
	 *
	 * @since  1.0.0
	 */
	public function add_options_page() {
	
		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'Paw2Heart Settings', 'paw2heart' ),
			__( 'Paw2Heart', 'paw2heart' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_settings_page' )
		);
	
	}
	
	/**
	 * Render the options page for plugin
	 *
	 * @since  1.0.0
	 */
	public function display_settings_page() {
		include_once "partials/paw2heart-admin-display.php";
	}
	
	public function register_settings() {
		// Add a General section
		$this->register_general_settings();
		
		// Add a Dog Breeds section
		$this->register_breeds_settings();
		
		// Add a Phrases section
		$this->register_phrases_settings();
		
		// Add a Frames section
		$this->register_frames_settings();
	}
	
	public function register_general_settings() {
		add_settings_section(
			$this->plugin_name . '_general',
			__( 'General', 'paw2heart' ),
			array( $this, $this->plugin_name . '_general_cb' ),
			$this->plugin_name
		);
		
		add_settings_field(
			$this->plugin_name . '_paypal_mode',
			__( 'PayPal Mode', 'paw2heart' ),
			array( $this, $this->plugin_name . '_paypal_mode_cb' ),
			$this->plugin_name,
			$this->plugin_name . '_general',
			array( 'label_for' => $this->plugin_name . '_paypal_mode' )
		);
		
		add_settings_field(
			$this->plugin_name . '_remove_tables',
			__( 'Remove data from database upon deactivation?', 'paw2heart' ),
			array( $this, $this->plugin_name . '_remove_tables_cb' ),
			$this->plugin_name,
			$this->plugin_name . '_general',
			array( 'label_for' => $this->plugin_name . '_remove_tables' )
		);
		
		register_setting( $this->plugin_name, $this->plugin_name . '_paypal_mode', array( $this, $this->plugin_name . '_sanitize_mode' ) );
		register_setting( $this->plugin_name, $this->plugin_name . '_remove_tables', 'intval' );
	}
	
	public function register_breeds_settings() {
		add_settings_section(
			$this->plugin_name . '_breeds',
			__( 'Dog Breeds', 'paw2heart' ),
			array( $this, $this->plugin_name . '_breeds_cb' ),
			$this->plugin_name
		);
		
		add_settings_field(
			$this->plugin_name . '_dog_breeds',
			__( 'Breeds', 'paw2heart' ),
			array( $this, $this->plugin_name . '_dog_breeds_cb' ),
			$this->plugin_name,
			$this->plugin_name . '_breeds',
			array( 'label_for' => $this->plugin_name . '_dog_breeds' )
		);
		
		register_setting( $this->plugin_name, $this->plugin_name . '_dog_breeds' );
		add_filter( 'pre_update_option_'.$this->plugin_name.'_dog_breeds', array($this, 'convert_textarea_to_arr'), 10, 2 );
	}
	
	public function register_frames_settings() {
		add_settings_section(
			$this->plugin_name . '_frames',
			__( 'Frames', 'paw2heart' ),
			array( $this, $this->plugin_name . '_frames_section_cb' ),
			$this->plugin_name
		);
		
		add_settings_field(
			$this->plugin_name . '_frames',
			__( 'Frames', 'paw2heart' ),
			array( $this, $this->plugin_name . '_frames_cb' ),
			$this->plugin_name,
			$this->plugin_name . '_frames',
			array( 'label_for' => $this->plugin_name . '_frames' )
		);
		
		register_setting( $this->plugin_name, $this->plugin_name . '_frames' );
		add_filter( 'pre_update_option_'.$this->plugin_name.'_frames', array($this, 'convert_textarea_to_arr'), 10, 2 );
	}
	
	public function register_phrases_settings() {
		add_settings_section(
			$this->plugin_name . '_phrases',
			__( 'Phrases', 'paw2heart' ),
			array( $this, $this->plugin_name . '_phrases_section_cb' ),
			$this->plugin_name
		);
		
		add_settings_field(
			$this->plugin_name . '_phrases',
			__( 'Paid Phrases', 'paw2heart' ),
			array( $this, $this->plugin_name . '_phrases_cb' ),
			$this->plugin_name,
			$this->plugin_name . '_phrases',
			array( 'label_for' => $this->plugin_name . '_phrases' )
		);
		
		register_setting( $this->plugin_name, $this->plugin_name . '_phrases' );
		add_filter( 'pre_update_option_'.$this->plugin_name.'_phrases', array($this, 'convert_textarea_to_arr'), 10, 2 );
	}
	
	/**
	 * Render the text for the general section
	 *
	 * @since  1.0.0
	 */
	public function paw2heart_general_cb() {
		//echo '<p>' . __( 'Please change the settings accordingly.', 'paw2heart' ) . '</p>';
	}
	
	/**
	 * Render the text for the breeds section
	 *
	 * @since  1.0.0
	 */
	public function paw2heart_breeds_cb() {
		//echo '<p>' . __( 'Please change the settings accordingly.', 'paw2heart' ) . '</p>';
	}
	
	/**
	 * Render the text for the frames section
	 *
	 * @since  1.0.0
	 */
	public function paw2heart_frames_section_cb() {
		//echo '<p>' . __( 'Please change the settings accordingly.', 'paw2heart' ) . '</p>';
	}
	
	/**
	 * Render the text for the phrases section
	 *
	 * @since  1.0.0
	 */
	public function paw2heart_phrases_section_cb() {
		//echo '<p>' . __( 'Please change the settings accordingly.', 'paw2heart' ) . '</p>';
	}
	
	/**
	 * Render the radio input field for the PayPal mode option
	 *
	 * @since  1.0.0
	 */
	public function paw2heart_paypal_mode_cb() {
		$mode = get_option( $this->plugin_name . '_paypal_mode' );
		?>
			<fieldset>
				<label>
					<input type="radio" name="<?php echo $this->plugin_name . '_paypal_mode' ?>" id="<?php echo $this->plugin_name . '_paypal_mode' ?>" value="sandbox" <?php checked( $mode, 'sandbox' ); ?>>
					<?php _e( 'Sandbox', 'paw2heart' ); ?>
				</label>
				<br>
				<label>
					<input type="radio" name="<?php echo $this->plugin_name . '_paypal_mode' ?>" value="prod" <?php checked( $mode, 'prod' ); ?>>
					<?php _e( 'Production', 'paw2heart' ); ?>
				</label>
			</fieldset>
		<?php
	}
	
	/**
	 * Render the checkbox input for whether or not database tables should be dropped upon deactivation
	 *
	 * @since  1.0.0
	 */
	public function paw2heart_remove_tables_cb() {
		$enabled = get_option( $this->plugin_name . '_remove_tables' );
		echo "<input type='checkbox' name='{$this->plugin_name}_remove_tables' id='{$this->plugin_name}_remove_tables' value='1'".checked( $enabled, '1', false )." />";
	}
	
	/**
	 * Render the textarea field for the dog breed option
	 *
	 * @since  1.0.0
	 */
	public function paw2heart_dog_breeds_cb() {
		$breeds = implode("\r", get_option( $this->plugin_name . '_dog_breeds' ));
		echo "<textarea name='{$this->plugin_name}_dog_breeds' id='{$this->plugin_name}_dog_breeds' cols='50' style='height: 150px'>$breeds</textarea><p><small>Each breed should be on its own line.</small></p>";
	}
	
	/**
	 * Render the textarea field for the paid phrases option
	 *
	 * @since  1.0.0
	 */
	public function paw2heart_phrases_cb() {
		$phrases = implode("\r", get_option( $this->plugin_name . '_phrases' ));
		echo "<textarea name='{$this->plugin_name}_phrases' id='{$this->plugin_name}_phrases' cols='50' style='height: 150px'>$phrases</textarea><p><small>Each phrase should be on its own line.</small></p>";
	}
	
	/**
	 * Render the textarea field for the paid phrases option
	 *
	 * @since  1.0.0
	 */
	public function paw2heart_frames_cb() {
		$frames = implode("\r", get_option( $this->plugin_name . '_frames' ));
		echo "<textarea name='{$this->plugin_name}_frames' id='{$this->plugin_name}_frames' cols='50' style='height: 150px'>$frames</textarea><p><small>Each frame name should be on its own line.</small></p>";
	}
	
	/**
	 * Sanitize the text PayPal mode value before being saved to database
	 *
	 * @param  string $mode $_POST value
	 * @since  1.0.0
	 * @return string           Sanitized value
	 */
	public function paw2heart_sanitize_mode( $mode ) {
		if ( in_array( $mode, array( 'prod', 'sandbox' ), true ) ) {
	        return $mode;
	    }
	}
	
	/**
	 * Convert the dog breeds textarea value into an array before saving to database
	 *
	 * @param  string $mode $_POST value
	 * @since  1.0.0
	 * @return string           Sanitized value
	 */
	public function convert_textarea_to_arr( $new_value, $old_value ) {
		return explode("\r", $new_value);
	}

}
